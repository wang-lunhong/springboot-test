package com.it.demo.controller;

import com.it.demo.entity.Emp;
import com.it.demo.entity.Student;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: Shen Qian
 * @Date : 20/10/2021
 * @Description:
 */
@RestController
public class StudentController {

    //返回昨天写的每一个用户个人信息
    @GetMapping("/StudentInfo")
    public  Object  StudentInfo()
    {
        Student student=new Student("1","张三",19,"男","高三二班");
        return  student;
    }

    //返回三个用户信息集合
    @GetMapping("/StudentInfoList")
    public  Object  StudentInfoList()
    {
        List<Student> studentList = new ArrayList<>();
        Student student1=new Student("2","lucy",19,"女","高三二班");
        Student student2=new Student("3","Tom",20,"男","高三一班");
        Student student3=new Student("4","Dave",18,"男","高三五班");
        studentList.add(student1);
        studentList.add(student2);
        studentList.add(student3);

        return  studentList;
    }



}
