package com.it.demo.controller;

import com.it.demo.entity.User;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class UserSQController {

    @RequestMapping("/function1")
    @ResponseBody
    public Object function1() {

        User user = new User();
        user.setId(1);
        user.setName("UserNameOne");
        user.setEmail("xx@xx.com");
        user.setPhone("18000000000");
        return user;
    }

    @RequestMapping("/function2")
    @ResponseBody
    public Object function2() {
        List<User> list = new ArrayList<>();
        User user = new User();
        user.setId(1);
        user.setName("UserNameOne");
        user.setEmail("xx@xx.com");
        user.setPhone("18000000000");

        list.add(user);
        user = new User();
        user.setId(2);
        user.setName("UserNameTwo");
        user.setEmail("xx@xx.com");
        user.setPhone("18000000000");

        list.add(user);
        user = new User();
        user.setId(3);
        user.setName("UserNameThree");
        user.setEmail("xx@xx.com");
        user.setPhone("18000000000");

        list.add(user);
        return list;
    }
}
