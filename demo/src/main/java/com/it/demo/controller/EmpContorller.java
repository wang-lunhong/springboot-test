package com.it.demo.controller;

import com.it.demo.entity.Emp;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class EmpContorller {

    @GetMapping("empTest.do")
    @ResponseBody
    public Object empTest(){
        Emp emp1 =new Emp(1,"Jacy",20,"female","1993992932293");

        return emp1;

    }

    @GetMapping("empListTest.do")
    @ResponseBody
    public Object empListTest(){
        List<Emp> empList = new ArrayList<>();
        empList.add(new Emp(1,"Jacy",20,"female","1993992932293"));
        empList.add(new Emp(2,"Mick",20,"male","9384089324"));
        empList.add(new Emp(3,"Lily",20,"female","31431"));
        return empList;
    }

}
