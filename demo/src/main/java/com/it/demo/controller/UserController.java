package com.it.demo.controller;

import com.it.demo.entity.User;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName: UserController
 * @Description:
 * @Author: 85351
 * @ADate: 2021/10/20 21:07
 */
@RestController
public class UserController {

    @RequestMapping("/user")
    public Object user(){
        User user = new User();
        user.setId(1);
        user.setName("张三");
        user.setPassword("helloPassword");
        user.setEmail("hello@java.com");
        user.setPhone("18190756324");
        return user;
    }

    @RequestMapping("/userList")
    public Object userList(){
        List<User> list = new ArrayList<>();

        User user1 = new User();
        user1.setId(1);
        user1.setName("张三");
        user1.setPassword("helloPassword");
        user1.setEmail("hello@java.com");
        user1.setPhone("18190756324");
        list.add(user1);

        User user2 = new User();
        user2.setId(1);
        user2.setName("李四");
        user2.setPassword("hhhaaaa");
        user2.setEmail("hello@java.com");
        user2.setPhone("18190751234");
        list.add(user2);

        User user3 = new User();
        user3.setId(1);
        user3.setName("赵小明");
        user3.setPassword("gitmaven");
        user3.setEmail("hello@java.com");
        user3.setPhone("18099885566");
        list.add(user3);

        return list;
    }
}
