package com.it.demo.entity;

/**
 * @Author: Shen Qian
 * @Date : 20/10/2021
 * @Description:
 */
public class Student {
    private String student_id ;
    private String student_name;
    private Integer age;
    private String  sex;
    private String  grade;

    public Student(String student_id, String student_name, Integer age, String sex, String grade) {
        this.student_id = student_id;
        this.student_name = student_name;
        this.age = age;
        this.sex = sex;
        this.grade = grade;
    }

    public String getStudent_id() {
        return student_id;
    }

    public void setStudent_id(String student_id) {
        this.student_id = student_id;
    }

    public String getStudent_name() {
        return student_name;
    }

    public void setStudent_name(String student_name) {
        this.student_name = student_name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    @java.lang.Override
    public java.lang.String toString() {
        return "Student{" +
                "student_id='" + student_id + '\'' +
                ", student_name='" + student_name + '\'' +
                ", age=" + age +
                ", sex='" + sex + '\'' +
                ", grade='" + grade + '\'' +
                '}';
    }
}
